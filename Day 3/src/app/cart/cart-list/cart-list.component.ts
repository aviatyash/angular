import { Component, OnInit } from '@angular/core';
import { Cart } from '../cart.model';

@Component({
  selector: 'app-cart-list',
  templateUrl: './cart-list.component.html',
  styleUrls: ['./cart-list.component.scss']
})
export class CartListComponent implements OnInit {
  carts: Cart[]=[
    new Cart('Cart1','Cart desc1'),
    new Cart('Cart2','Cart desc2'),
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
