import { Customer } from './customer';
 
 
export class CustomerserviceComponent{
 
    public getCustomers() {
 
        let customers:Customer[];
 
        customers=[
            new Customer(1,'Customer 1'),
            new Customer(2,'Customer 2'),
            new Customer(3,'Customer 3')
        ]
 
        return customers;               
    }
 
 
    public getProduct(_id:any) {
        let products:Customer[]=this.getCustomers();
        return products.find(p => p.customerID==_id);
    }
 
 
}
