//import { Observable } from 'rxjs/Observable';
import { Product } from './product';
 
 
export class ProductserviceComponent{
 
    public getProducts() {
 
        let products:Product[];
 
        products=[
            new Product(1,'User 1'),
            new Product(2,'User 2'),
            new Product(3,'User 3')
        ]
 
        return products;               
    }
 
 
    public getProduct(_id:any) {
        let products:Product[]=this.getProducts();
        return products.find(p => p.productID==_id);
    }
 
 
}
