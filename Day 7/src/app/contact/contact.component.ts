import { Component, OnInit } from '@angular/core';
import { CustomerserviceComponent } from '../customerservice/customerservice.component';
import { Customer } from '../customerservice/customer';
@Component({
  selector: 'app-product',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  customers!:Customer[];
   
  constructor(private customerService:CustomerserviceComponent){
  }

  ngOnInit() {
    this.customers=this.customerService.getCustomers();
  }

}