import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../productservice/product';
import { ProductserviceComponent } from '../productservice/productservice.component';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit {

  product: Product | undefined;
  id:any;
  constructor(private _Activatedroute:ActivatedRoute,
    private _router:Router,
    private _productService:ProductserviceComponent) { }

  sub:any;
    ngOnInit(){
      this.sub=this._Activatedroute.paramMap.subscribe(params=>{
        console.log(params);
        this.id=params.get('id');
        let products=this._productService.getProducts();
        this.product=products.find(p=>p.productID==this.id)
      })
  }
  ngOnDestroy(){
    this.sub.unsubscribe();
  }

  onBack():void{
    this._router.navigate(['product'])
  }

}
