import { Routes } from "@angular/router"
import { HomeComponent } from "./home/home.component"
import { ContactComponent } from "./contact/contact.component"
import { ErrorComponent } from "./error/error.component"
import { ProductComponent } from "./product/product.component"
import { ProductDetailComponent } from "./product-detail/product-detail.component"
import { ContactDetailComponent } from "./contact-detail/contact-detail.component"
export const appRoutes:Routes=[
    { path: 'home', component: HomeComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'product', component: ProductComponent },
    {path:'product/:id',component:ProductDetailComponent},
    {path:'contact/:id',component:ContactDetailComponent},
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: '**', component: ErrorComponent }
]