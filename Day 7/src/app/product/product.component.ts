import { Component, OnInit } from '@angular/core';
import { ProductserviceComponent } from '../productservice/productservice.component';
import { Product } from '../productservice/product';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  products!:Product[];
   
  constructor(private productService:ProductserviceComponent){
  }

  ngOnInit() {
    this.products=this.productService.getProducts();
  }

}
