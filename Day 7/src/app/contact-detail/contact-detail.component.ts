import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';
import { CustomerserviceComponent } from '../customerservice/customerservice.component';
import { Customer } from '../customerservice/customer';
@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.css']
})
export class ContactDetailComponent implements OnInit {

  customer:Customer | undefined;
  id:any;
  constructor(private _Activatedroute:ActivatedRoute,
    private _router:Router,
    private _customerService:CustomerserviceComponent) { }

    sub:any;

  ngOnInit() {
    this.sub=this._Activatedroute.paramMap.subscribe(params=>{
      console.log(params);
      this.id=params.get('id');
      let customers=this._customerService.getCustomers();
      this.customer=customers.find(p=>p.customerID==this.id)
    });
  }
  ngOnDestroy(){
    this.sub.unsubscribe();
  }
onBack():void{
  this._router.navigate(['customer'])
}
}
