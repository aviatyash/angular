import { Component } from '@angular/core';
import { FormGroup,FormControl,Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-assign-sixbb';
  submitted= false;
  contactForm=new FormGroup({
    email:new FormControl('',[Validators.email,Validators.required,Validators.minLength(5)]),
    password:new FormControl('',[Validators.required,Validators.minLength(5)]),
    status:new FormControl('active'),
  })
  onSubmit(){
    console.log(this.contactForm.value)
    this.submitted=true
  }
  updatePassword(){
    this.contactForm.patchValue({
      password:'admin1234'
    })
  }
}
