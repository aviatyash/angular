import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-assign-sixa';
  user={
    email:"",
    password:'',
    status:'',
  }
  submitted= false;
  emailvalid:any=false;
  passwordvalid:any=false;
  enablebutton=true;

  defaultStatus:any='active'
  @ViewChild('f')
  formValues!: NgForm;
  submitForm(form:NgForm){
    this.submitted=true;
    this.user.email=this.formValues.value.email;
    this.user.password=this.formValues.value.password;
    this.user.status=this.formValues.value.status;
    console.log(this.user)
    this.formValues.reset();
  }
  updatePass(){
    const updatePassword='admin123';
    this.formValues.form.patchValue({
      password:updatePassword,
    })
  }
  inputChange(){
    if(this.formValues.value.email && this.formValues.value.password !== "" ){
     this.emailvalid=true;
     this.passwordvalid=true;
     this.enablebutton=false;
     console.log(this.enablebutton)
    }
  }
}
