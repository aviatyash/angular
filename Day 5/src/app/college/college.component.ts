import { Component, OnInit } from '@angular/core';
import { SchoolStudentsService } from '../school.services';
import { SchoolStudents } from '../schoolstudents';

@Component({
  selector: 'app-college',
  templateUrl: './college.component.html',
  styleUrls: ['./college.component.css']
})
export class CollegeComponent implements OnInit {

  students!:SchoolStudents[];
  schoolstudentservice:any;
  schoolstudentslist:number=0;
  constructor() {
    this.schoolstudentservice=new SchoolStudentsService()
   }
   getStudents(){
     this.students=this.schoolstudentservice.getStudents();
     this.schoolstudentservice=true;
     this.schoolstudentslist+1;
     console.log(this.schoolstudentslist)
   }

  ngOnInit(): void {
  }

}
