import { Component, OnInit } from '@angular/core';
import { CollegeStudentsService } from '../college.service';
import { CollegeStudents } from '../collegestudents';
@Component({
  selector: 'app-school',
  templateUrl: './school.component.html',
  styleUrls: ['./school.component.css']
})
export class SchoolComponent implements OnInit {


  collegestudents!:CollegeStudents[];
  collegestudentservice:any;
  collegestudentslist!:true;
  constructor() {
    this.collegestudentservice=new CollegeStudentsService()
   }
   getCollegeStudents(){
     this.collegestudents=this.collegestudentservice.getCollegeStudents();
   }

  ngOnInit(): void {
  }

}
