import { Component, OnInit } from '@angular/core';

@Component({
  selector: '.app-failure',
  templateUrl: './failure.component.html',
  styleUrls: ['./failure.component.scss'],
  template:'<app-root></app-root>',
  styles:[
    `
    h1{
      color:red;
    }
    `,
  ],
})
export class FailureComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
