import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-odd',
  templateUrl: './odd.component.html',
  styleUrls: ['./odd.component.scss']
})
export class OddComponent implements OnInit {
  @Input()
  odd:number=-1;
  countclick:any;
  constructor() {
    setInterval(() => {
      if(this.countclick%2!==0)
      this.odd=this.odd+2
    }, 1000);
   }

  ngOnInit(): void {
  }

}
