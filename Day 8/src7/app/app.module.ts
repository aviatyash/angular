import { FirebaseService } from './services/firebase.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AngularFireModule} from '@angular/fire/compat';
import { HomeComponent } from './home/home.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
   AngularFireModule.initializeApp({
  apiKey: "AIzaSyBNpi65ZvRdKMPSxVZW6jlsFi19ZMeEcrI",
  authDomain: "fir-angular-auth-2d4d1.firebaseapp.com",
  projectId: "fir-angular-auth-2d4d1",
  storageBucket: "fir-angular-auth-2d4d1.appspot.com",
  messagingSenderId: "471015068585",
  appId: "1:471015068585:web:8b5a5c1d7df5f99bb72ab8"
})
  ],
  providers: [FirebaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
